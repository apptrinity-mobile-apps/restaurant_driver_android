import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/screens/dashboard/ui/dashboard_main.dart';
import 'package:zingdriver/screens/dashboard/viewModel/dashboard_view_model.dart';
import 'package:zingdriver/screens/login/ui/login.dart';
import 'package:zingdriver/screens/login/viewModel/login_view_model.dart';
import 'package:zingdriver/screens/orderAcceptance/ui/acceptorderscreen.dart';
import 'package:zingdriver/screens/orderAcceptance/viewModel/accept_order_view_model.dart';
import 'package:zingdriver/screens/orderDetails/viewModel/order_detailed_view_model.dart';
import 'package:zingdriver/screens/ordersHistory/viewModels/orders_history_view_model.dart';
import 'package:zingdriver/screens/splash/splash_screen.dart';
import 'package:zingdriver/screens/tracking/ui/pickuporderscreen.dart';
import 'package:zingdriver/screens/tracking/viewModel/tracking_view_model.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

AndroidNotificationChannel channel;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print(
      'Handling a background message ${message?.notification?.body}=====${message?.notification?.title}');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await FirebaseMessaging.instance.getToken().then((value) {
    print("token: $value");
  });
  channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginViewModel()),
        ChangeNotifierProvider(create: (context) => DashBoardViewModel()),
        ChangeNotifierProvider(create: (context) => TrackingViewModel()),
        ChangeNotifierProvider(create: (context) => AcceptOrderViewModel()),
        ChangeNotifierProvider(create: (context) => OrdersHistoryViewModel()),
        ChangeNotifierProvider(create: (context) => OrderDetailedViewModel()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MyApp(),
      )));
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      print(
          'onMessage getInitialMessage $message ${message?.notification?.body}=====${message?.notification?.title}');
      if (message != null) {
        Map<String, dynamic> data = message.data;
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AcceptOrderScreen(
                  orderId: data["orderId"],
                  deliveryRecordId: data["deliveryRecordId"],
                )));
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(
          'onMessage $message ${message?.notification?.body}=====${message?.notification?.title} ${message.data}');
      RemoteNotification notification = message.notification;
      Map<String, dynamic> data = message.data;
      AndroidNotification android = message.notification?.android;
        if (notification != null && android != null) {
          flutterLocalNotificationsPlugin.show(
              notification.hashCode,
              notification.title,
              notification.body,
              NotificationDetails(
                android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  channel.description,
                  // TODO add a proper drawable resource to android, for now using one that already exists in example app.
                  icon: 'launch_background',
                  playSound: true,
                ),
              ));
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    AcceptOrderScreen(
                      orderId: data["orderId"],
                      deliveryRecordId: data["deliveryRecordId"],
                    )));
          });
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('onMessage OpenedApp $message ${message?.notification?.body}=====${message?.notification?.title}');
      if (message != null) {
        Map<String, dynamic> data = message.data;
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AcceptOrderScreen(
                  orderId: data["orderId"],
                  deliveryRecordId: data["deliveryRecordId"],
                )));
      }
    });

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Driver',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/HomeScreen': (BuildContext context) => MyDashboard(),
        '/LoginScreen': (BuildContext context) => LoginPage(),
      },
    );
  }
}
