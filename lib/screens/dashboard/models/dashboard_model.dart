class DashBoardModel {
  DriverData driverData;
  int responseStatus;
  String result;

  DashBoardModel({this.driverData, this.responseStatus, this.result});

  DashBoardModel.fromJson(Map<String, dynamic> json) {
    driverData = json['driverData'] != null
        ? new DriverData.fromJson(json['driverData'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.driverData != null) {
      data['driverData'] = this.driverData.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DriverData {
  int cancelled;
  int completed;
  double driverRating;
  bool isDriverAvailable;
  LastDeliveryLocation lastDeliveryLocation;
  String lastDeliveryLocationAddress;
  LastDeliveryLocation lastPickupLocation;
  String lastPickupLocationAddress;
  int totalOrders;

  DriverData(
      {this.cancelled,
        this.completed,
        this.driverRating,
        this.isDriverAvailable,
        this.lastDeliveryLocation,
        this.lastDeliveryLocationAddress,
        this.lastPickupLocation,
        this.lastPickupLocationAddress,
        this.totalOrders});

  DriverData.fromJson(Map<String, dynamic> json) {
    cancelled = json['cancelled'];
    completed = json['completed'];
    driverRating = json['driver_rating'];
    isDriverAvailable = json['isDriverAvailable'];
    lastDeliveryLocation = json['lastDeliveryLocation'] != null
        ? new LastDeliveryLocation.fromJson(json['lastDeliveryLocation'])
        : null;
    lastDeliveryLocationAddress = json['lastDeliveryLocationAddress'];
    lastPickupLocation = json['lastPickupLocation'] != null
        ? new LastDeliveryLocation.fromJson(json['lastPickupLocation'])
        : null;
    lastPickupLocationAddress = json['lastPickupLocationAddress'];
    totalOrders = json['totalOrders'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cancelled'] = this.cancelled;
    data['completed'] = this.completed;
    data['driver_rating'] = this.driverRating;
    data['isDriverAvailable'] = this.isDriverAvailable;
    if (this.lastDeliveryLocation != null) {
      data['lastDeliveryLocation'] = this.lastDeliveryLocation.toJson();
    }
    data['lastDeliveryLocationAddress'] = this.lastDeliveryLocationAddress;
    if (this.lastPickupLocation != null) {
      data['lastPickupLocation'] = this.lastPickupLocation.toJson();
    }
    data['lastPickupLocationAddress'] = this.lastPickupLocationAddress;
    data['totalOrders'] = this.totalOrders;
    return data;
  }
}

class LastDeliveryLocation {
  List<double> coordinates;
  String type;

  LastDeliveryLocation({this.coordinates, this.type});

  LastDeliveryLocation.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'].cast<double>();
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['coordinates'] = this.coordinates;
    data['type'] = this.type;
    return data;
  }
}
