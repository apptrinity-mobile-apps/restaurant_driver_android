import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/screens/dashboard/models/dashboard_model.dart';
import 'package:zingdriver/screens/dashboard/viewModel/dashboard_view_model.dart';
import 'package:zingdriver/screens/orderAcceptance/ui/acceptorderscreen.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/shared_preferences.dart';

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key key}) : super(key: key);

  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  double mRating = 0.0;
  String mName = "",
      mEmployeeId = "",
      onlineStatus = "",
      mLastPickUpAddress = "",
      mProfilePic = "",
      mapStyle = "";
  int mCompleted = 0, mCancelled = 0, mTotal = 0, switchedStatus = 0;
  DashBoardViewModel viewModel;
  LatLng mLastPickUpLatLng, mLastDeliveryLatLng;
  CameraPosition mCameraPosition;
  Completer<GoogleMapController> _controller = Completer();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};
  final List<LatLng> mLatLngPoints = <LatLng>[];
  bool isSwitchedOn = false;

  @override
  void initState() {
    viewModel = Provider.of<DashBoardViewModel>(context, listen: false);
    rootBundle.loadString('map_style/map_style_silver.txt').then((string) {
      mapStyle = string;
    });
    setState(() {
      mLastPickUpLatLng = LatLng(0.0, 0.0);
      mCameraPosition =
          CameraPosition(target: mLastPickUpLatLng, zoom: mapZoom);
    });
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mEmployeeId = value.employeeDetails.id;
        print("mEmployeeId" + mEmployeeId);
        mName =
            "${value.employeeDetails.firstName} ${value.employeeDetails.lastName}";
        mProfilePic = value.employeeDetails.profilePic;
        viewModel.getDashBoardDetailsApi(mEmployeeId);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<DashBoardViewModel>(context, listen: true);
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          backgroundColor: Colors.white,
          title: new Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Text(
                titleDashboard,
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w700,
                    color: kToolbarTitleColor),
              )),
          centerTitle: false,
          /*actions: [
            new Padding(
              padding: const EdgeInsets.only(right: 10),
              child: new Container(
                  alignment: Alignment.centerRight,
                  height: 48.0,
                  width: 48.0,
                  child: new GestureDetector(
                    onTap: () {},
                    child: IconButton(
                      icon:
                          new Icon(Icons.menu, size: 22.0, color: Colors.grey),
                      onPressed: () {},
                    ),
                  )),
            )
          ],*/
        ),
        body: dashboardViews(viewModel));
  }

  Widget dashboardViews(DashBoardViewModel viewModel) {
    bool hasData = viewModel.isHavingData;
    DashBoardModel dashBoardModel;
    if (hasData) {
      if (viewModel.dashBoardModel != null) {
        setState(() {
          dashBoardModel = viewModel.dashBoardModel;
        });
        if (dashBoardModel.responseStatus == 1) {
          print(dashBoardModel.driverData);
          setState(() {
            mRating = dashBoardModel.driverData.driverRating;
            isSwitchedOn = dashBoardModel.driverData.isDriverAvailable;
            onlineStatus = isSwitchedOn ? online : offline;
            mCompleted = dashBoardModel.driverData.completed;
            mCancelled = dashBoardModel.driverData.cancelled;
            mTotal = dashBoardModel.driverData.totalOrders;
            if (isSwitchedOn) {
              switchedStatus = 1;
            } else {
              switchedStatus = 0;
            }
            mLastPickUpAddress =
                dashBoardModel.driverData.lastPickupLocationAddress == null
                    ? ""
                    : dashBoardModel.driverData.lastPickupLocationAddress;
            if (dashBoardModel.driverData.lastPickupLocation != null) {
              mLastPickUpLatLng = LatLng(
                  dashBoardModel.driverData.lastPickupLocation.coordinates[0],
                  dashBoardModel.driverData.lastPickupLocation.coordinates[1]);
              mLastDeliveryLatLng = LatLng(
                  dashBoardModel.driverData.lastDeliveryLocation.coordinates[0],
                  dashBoardModel
                      .driverData.lastDeliveryLocation.coordinates[1]);
            } else {
              mLastPickUpLatLng = LatLng(0.00, 0.00);
              mLastDeliveryLatLng = LatLng(0.00, 0.00);
            }
            createPickUpMarker();
            //createDeliveryMarker();
            //createPolyline();
            mCameraPosition =
                CameraPosition(target: mLastPickUpLatLng, zoom: mapZoom);
            updateMapLocation();
            SessionManager().online(isSwitchedOn);
          });
        }
      } else {
        print("dashboard called");
        Fluttertoast.showToast(msg: tryAgain);
      }
    }

    return SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
      new Container(
          margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
          child: Card(
            elevation: 3,
            child: new Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                new Container(
                  height: 100.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Container(
                                margin:
                                    const EdgeInsets.only(left: 10, right: 10),
                                width: 60.0,
                                height: 60.0,
                                child: CircleAvatar(
                                    backgroundColor: Colors.lightBlueAccent,
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(
                                            color: Colors.black,
                                          ),
                                      imageUrl: mProfilePic,
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              'images/profile_default.png'),
                                      fit: BoxFit.fill,
                                      filterQuality: FilterQuality.medium,
                                    ))),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  mName,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      mRating.toString(),
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: CupertinoColors.black),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    RatingBar.builder(
                                      initialRating: mRating,
                                      minRating: 1,
                                      direction: Axis.horizontal,
                                      allowHalfRating: false,
                                      itemCount: 5,
                                      itemSize: 20,
                                      itemPadding:
                                          EdgeInsets.symmetric(horizontal: 0.0),
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Colors.orangeAccent,
                                      ),
                                      ignoreGestures: true,
                                      updateOnDrag: true,
                                      unratedColor: Colors.grey,
                                      onRatingUpdate: (rating) {
                                        setState(() {
                                          mRating = rating;
                                        });
                                      },
                                    )
                                    //Icon(Icons.star_border, size: 25.0),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    new Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            Switch(
                              value: isSwitchedOn,
                              onChanged: (value) async {
                                setState(() {
                                  isSwitchedOn = value;
                                  print(isSwitchedOn);
                                  if (isSwitchedOn) {
                                    switchedStatus = 1;
                                  } else {
                                    switchedStatus = 0;
                                  }
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => AcceptOrderScreen()));
                                });
                                await viewModel
                                    .updateOnlineStatusApi(
                                        mEmployeeId, switchedStatus)
                                    .then((value) {
                                  if (value) {
                                    SessionManager().online(isSwitchedOn);
                                    viewModel
                                        .getDashBoardDetailsApi(mEmployeeId);
                                  }
                                });
                              },
                              activeTrackColor: Colors.lightBlueAccent,
                              inactiveTrackColor: Colors.black12,
                              activeColor: Colors.blue,
                            ),
                            Text(
                              onlineStatus = isSwitchedOn ? online : offline,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black,
                                  fontSize: 13),
                            ),
                          ],
                        )),
                  ],
                )
              ],
            ),
          )),
      // TODO uncomment in future
      /* new Container(
          margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
          height: 180,
          child: Card(
            elevation: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: 10),
                    Text(
                      "Goal Buddy -",
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w700,
                          color: CupertinoColors.black),
                    ),
                    Text(
                      "Achieve your goal!",
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: CupertinoColors.black),
                    ),

                    //Icon(Icons.star_border, size: 25.0),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Container(
                        //margin: const EdgeInsets.only(left:10,right:10),
                        width: 90.0,
                        height: 90.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            color: imagebg,
                            image: new DecorationImage(
                              fit: BoxFit.none,
                              image: new AssetImage('images/gallery.png'),
                            ))),
                    Container(
                      width: 90.0,
                      height: 90.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: imagebg,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "10",
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          ),
                          Text(
                            "Order's Left",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          ),

                          //Icon(Icons.star_border, size: 25.0),
                        ],
                      ),
                    ),
                    Container(
                      width: 90.0,
                      height: 90.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: imagebg,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "68 %",
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          ),
                          Text(
                            "Completion",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          ),

                          //Icon(Icons.star_border, size: 25.0),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )),*/
      new Container(
          margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
          height: 100,
          child: Card(
            elevation: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: 90.0,
                      height: 90.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            mTotal.toString(),
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          ),
                          Text(
                            totalOrders,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        height: 60,
                        child: VerticalDivider(
                          color: kToolbarTitleColor,
                          thickness: 0.5,
                        )),
                    Container(
                      width: 90.0,
                      height: 90.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            mCancelled.toString(),
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          ),
                          Text(
                            cancelled,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        height: 60,
                        child: VerticalDivider(
                          color: kToolbarTitleColor,
                          thickness: 0.5,
                        )),
                    Container(
                      width: 90.0,
                      height: 90.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            mCompleted.toString(),
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          ),
                          Text(
                            completed,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )),
      new Container(
          margin: const EdgeInsets.only(top: 20, left: 20, right: 15),
          child: Text(
            "Last order",
            style: TextStyle(
                fontSize: 18,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w700,
                color: CupertinoColors.black),
          )),
      new Container(
          margin: const EdgeInsets.only(top: 10, left: 15, right: 15),
          child: Card(
            elevation: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                    margin: const EdgeInsets.only(top: 10, left: 10, right: 0),
                    child: Text(
                      pickUp,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w600,
                          color: CupertinoColors.black),
                    )),
                Container(
                    margin: const EdgeInsets.only(top: 3, left: 10, right: 0),
                    child: Text(
                      mLastPickUpAddress,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: CupertinoColors.black),
                    )),
                Container(
                  margin: const EdgeInsets.only(
                      top: 10, left: 10, right: 10, bottom: 10),
                  height: 150,
                  child: ClipRRect(
                    child: GoogleMap(
                      initialCameraPosition: mCameraPosition,
                      mapType: MapType.normal,
                      zoomControlsEnabled: false,
                      zoomGesturesEnabled: false,
                      compassEnabled: false,
                      mapToolbarEnabled: false,
                      scrollGesturesEnabled: false,
                      myLocationEnabled: false,
                      rotateGesturesEnabled: false,
                      myLocationButtonEnabled: false,
                      indoorViewEnabled: false,
                      tiltGesturesEnabled: false,
                      trafficEnabled: false,
                      buildingsEnabled: true,
                      onMapCreated: (GoogleMapController controller) {
                        controller.setMapStyle(mapStyle);
                        _controller.complete(controller);
                      },
                      markers: Set<Marker>.of(markers.values),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                )
              ],
            ),
          )),
    ]));
  }

  Future<void> updateMapLocation() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(mCameraPosition));
  }

  void navigateToOrdersScreen() {
    if (isSwitchedOn) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => AcceptOrderScreen()));
    }
  }

  void createPickUpMarker() {
    markers.clear();
    final String markerIdVal = 'marker_pick_up';
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = Marker(
      markerId: markerId,
      position: mLastPickUpLatLng,
    );
    setState(() {
      markers[markerId] = marker;
    });
  }

  void createDeliveryMarker() {
    final String markerIdVal = 'marker_delivery';
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = Marker(
      markerId: markerId,
      position: mLastDeliveryLatLng,
    );
    setState(() {
      markers[markerId] = marker;
    });
  }

  void createPolyline() {
    final String polylineIdVal = 'polyline_id_';
    final PolylineId polylineId = PolylineId(polylineIdVal);
    final Polyline polyline = Polyline(
      polylineId: polylineId,
      consumeTapEvents: false,
      color: Colors.orange,
      width: 5,
      points: mLatLngPoints,
    );
    setState(() {
      polylines[polylineId] = polyline;
    });
  }
}
