import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zingdriver/screens/dashboard/ui/dashboard_frag.dart';
import 'package:zingdriver/screens/dashboard/ui/profile_frag.dart';
import 'package:zingdriver/screens/dashboard/ui/settings_frag.dart';
import 'package:zingdriver/utils/all_constans.dart';

class MyDashboard extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage() : super();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isSwitchedOn = false;
  int _selectedIndex = 0;

  final List<Widget> pages = [
    DashBoardScreen(
      key: PageStorageKey('Page1'),
    ),
    ProfileScreen(
      key: PageStorageKey('Page2'),
    ),
    SettingsScreen(key: PageStorageKey('Page3'),
    )
  ];

  @override
  void initState() {
    setState(() {
       _selectedIndex = 0;
    });
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: dashboard_bg,
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
            canvasColor: bottom_nv_bg,
            primaryColor: bottom_nv_bg,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.yellow))),
        // sets the inactive color of the `BottomNavigationBar`
        child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(0),
              topRight: Radius.circular(0),
              bottomRight: Radius.circular(0),
              bottomLeft: Radius.circular(0),
            ),
            child: BottomNavigationBar(
              showUnselectedLabels: false,
              unselectedItemColor: Colors.grey,
              selectedItemColor: Colors.white,
              selectedFontSize: 12,
              unselectedFontSize: 12,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.grid_view,
                    color: Colors.white,
                  ),
                  activeIcon: Icon(
                    Icons.grid_view,
                    color: Colors.blue,
                  ),
                  label: '',
                  tooltip: titleDashboard,
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.person_outline_sharp,
                    color: Colors.white,
                  ),
                  activeIcon: Icon(
                    Icons.person_outline_sharp,
                    color: Colors.blue,
                  ),
                  label: '',
                  tooltip: titleProfile,
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                  activeIcon: Icon(
                    Icons.settings,
                    color: Colors.blue,
                  ),
                  label: '',
                  tooltip: titleSettings,
                ),
              ],
              currentIndex: _selectedIndex,
              //New
              onTap: _onItemTapped,
              type: BottomNavigationBarType.fixed,
            )),
      ),
      body: /*IndexedStack(
        index: _selectedIndex,
        children: [DashBoardScreen(), ProfileScreen(), SettingsScreen()],
      )*/
      pages[_selectedIndex],
    );
  }
}
