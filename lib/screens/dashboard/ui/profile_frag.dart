
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zingdriver/utils/all_constans.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        title: new Padding(
            padding: const EdgeInsets.only(left: 0),
            child: Text(
              titleProfile,
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                  color: kToolbarTitleColor),
            )),
        centerTitle: false,
      ),
    );
  }
}
