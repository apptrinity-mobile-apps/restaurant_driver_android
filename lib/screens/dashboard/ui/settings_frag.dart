import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/screens/dashboard/viewModel/dashboard_view_model.dart';
import 'package:zingdriver/screens/login/ui/login.dart';
import 'package:zingdriver/screens/ordersHistory/ui/orders_history_screen.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/dialogs.dart';
import 'package:zingdriver/utils/shared_preferences.dart';
import 'package:zingdriver/utils/sizeconfig.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  String mEmployeeId = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mEmployeeId = value.employeeDetails.id;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<DashBoardViewModel>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        title: new Padding(
            padding: const EdgeInsets.only(left: 0),
            child: Text(
              titleSettings,
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                  color: kToolbarTitleColor),
            )),
        centerTitle: false,
      ),
      body: Material(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => OrdersHistoryScreen()));
              },
              child: Container(
                  width: SizeConfig.screenWidth,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                        child: Text(
                          titleDeliveryBox,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w500,
                              color: kToolbarTitleColor),
                        ),
                      ),
                      Container(
                          child: Divider(
                        height: 0,
                        color: kToolbarTitleColor,
                        thickness: 0.3,
                      )),
                    ],
                  )),
            ),
            InkWell(
              onTap: () {
                logout(viewModel);
              },
              child: Container(
                  width: SizeConfig.screenWidth,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                        child: Text(
                          titleLogout,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w500,
                              color: kToolbarTitleColor),
                        ),
                      ),
                      Container(
                          child: Divider(
                        height: 0,
                        color: kToolbarTitleColor,
                        thickness: 0.3,
                      )),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  void logout(DashBoardViewModel viewModel) {
    Dialogs.showLoadingDialog(context, pleaseWait, _keyLoader);
    viewModel.logoutApi(mEmployeeId).then((value) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      if (value) {
        if (viewModel.basicResponse.responseStatus == 0) {
          Fluttertoast.showToast(msg: viewModel.basicResponse.result);
        } else if (viewModel.basicResponse.responseStatus == 1) {
          Fluttertoast.showToast(msg: viewModel.basicResponse.result);
          SessionManager().login(false);
          SessionManager().saveUserDetails("");
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
              (Route<dynamic> route) => false);
        } else if (viewModel.basicResponse.responseStatus == 2) {
          Fluttertoast.showToast(msg: viewModel.basicResponse.result);
        }
      } else {
        Fluttertoast.showToast(msg: tryAgain);
      }
    });
  }
}
