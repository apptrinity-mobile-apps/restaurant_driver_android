import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:zingdriver/screens/dashboard/models/dashboard_model.dart';
import 'package:zingdriver/services/basic_response.dart';
import 'package:zingdriver/services/repositories.dart';

class DashBoardViewModel with ChangeNotifier {
  DashBoardViewModel();

  bool _isHavingData = false;

  bool get isHavingData => _isHavingData;
  DashBoardModel _dashBoardModel;
  BasicResponse _basicResponse;

  DashBoardModel get dashBoardModel {
    return _dashBoardModel;
  }
  BasicResponse get basicResponse {
    return _basicResponse;
  }

  Future<bool> getDashBoardDetailsApi(String employeeId) async {
    _isHavingData = false;
    _dashBoardModel = DashBoardModel();
    notifyListeners();
    try {
      dynamic response = await Repository().dashboardDetails(employeeId);
      if (response != null) {
        _dashBoardModel = DashBoardModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    notifyListeners();
    return _isHavingData;
  }

  Future<bool> updateOnlineStatusApi(String employeeId, int status) async {
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response =
          await Repository().updateOnlineStatus(employeeId, status);
      if (response != null) {
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    notifyListeners();
    return _isHavingData;
  }

  Future<bool> logoutApi(String employeeId) async {
    _isHavingData = false;
    _basicResponse = BasicResponse();
    notifyListeners();
    try {
      dynamic response =
          await Repository().logout(employeeId);
      if (response != null) {
        _basicResponse = BasicResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    notifyListeners();
    return _isHavingData;
  }
}
