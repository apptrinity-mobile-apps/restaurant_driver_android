class LoginModel {
  EmployeeDetails employeeDetails;
  int responseStatus;
  String result;

  LoginModel({this.employeeDetails, this.responseStatus, this.result});

  LoginModel.fromJson(Map<String, dynamic> json) {
    employeeDetails = json['employeeDetails'] != null
        ? new EmployeeDetails.fromJson(json['employeeDetails'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.employeeDetails != null) {
      data['employeeDetails'] = this.employeeDetails.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class EmployeeDetails {
  String empId;
  String firstName;
  String id;
  String lastName;
  String profilePic;
  String restaurantId;
  String uniqueNumber;

  EmployeeDetails(
      {this.empId,
        this.firstName,
        this.id,
        this.lastName,
        this.profilePic,
        this.restaurantId,
        this.uniqueNumber});

  EmployeeDetails.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    firstName = json['firstName'];
    id = json['id'];
    lastName = json['lastName'];
    profilePic = json['profilePic'];
    restaurantId = json['restaurantId'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['firstName'] = this.firstName;
    data['id'] = this.id;
    data['lastName'] = this.lastName;
    data['profilePic'] = this.profilePic == null ? "" : data['profilePic'];
    data['restaurantId'] = this.restaurantId;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}
