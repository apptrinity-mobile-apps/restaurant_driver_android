import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'package:zingdriver/screens/dashboard/ui/dashboard_main.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/shared_preferences.dart';
import 'package:zingdriver/utils/validateemail.dart';
import 'package:device_info/device_info.dart';
import 'package:zingdriver/screens/login/viewModel/login_view_model.dart';
import 'package:zingdriver/screens/login/models/login_model.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _signin_username = TextEditingController();
  TextEditingController _signin_password = TextEditingController();
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();

  LoginViewModel loginViewModel;
  String typeOfOS = "";
  String deviceName = "";
  String deviceToken = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    loginViewModel = Provider.of<LoginViewModel>(context, listen: false);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(child: _buildLoginFields()),
      ),
    );
  }

  Widget _buildLoginFields() {
    return Container(
      margin: EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0, bottom: 0.0),
      child: SingleChildScrollView(
          child: new Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 60.0,
            ),
            _buildEmailField(),
            SizedBox(
              height: 10.0,
            ),
            _buildPasswordField(),
            Padding(
              padding: EdgeInsets.only(top: 10, right: 20),
              child: Text(
                'Forgot Password?',
                style: TextStyle(
                    fontSize: 14,
                    color: bottom_nv_bg,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.end,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            _buildSubmitButton(),
            SizedBox(
              height: 25.0,
            ),
            _buildRegisterText(),
          ],
        ),
      )),
    );
  }

  Widget _buildEmailField() {
    return Container(
        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
        child: TextFormField(
            autocorrect: true,
            validator: validateEmail,
            controller: _signin_username,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              prefixIcon: Padding(
                  padding: const EdgeInsets.all(0),
                  child: IconButton(
                    icon: Image.asset(
                      'images/username.png',
                      width: 20,
                      height: 20,
                    ),
                    onPressed: () {},
                  )),
              hintText: 'User Name',
              hintStyle: TextStyle(fontSize: 14.0, color: bottom_nv_bg),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: bottom_nv_bg),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: bottom_nv_bg),
              ),
            )));
  }

  Widget _buildPasswordField() {
    return Container(
        margin: EdgeInsets.only(left: 20, top: 0, right: 20),
        child: TextFormField(
            autocorrect: true,
            validator: (value) =>
                value.isEmpty ? 'Password cannot be blank' : null,
            controller: _signin_password,
            obscureText: true,
            decoration: InputDecoration(
              prefixIcon: Padding(
                  padding: const EdgeInsets.all(0),
                  child: IconButton(
                    icon: Image.asset(
                      'images/password.png',
                      width: 20,
                      height: 20,
                    ),
                    onPressed: () {},
                  )),
              hintText: 'Password',
              hintStyle: TextStyle(fontSize: 14.0, color: bottom_nv_bg),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: bottom_nv_bg),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: bottom_nv_bg),
              ),
            )));
  }

  Widget _buildSubmitButton() {
    return Container(
        margin: EdgeInsets.only(left: 20, top: 10, right: 20),
        height: 56,
        padding: EdgeInsets.all(5),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: bottom_nv_bg,
            elevation: 3,
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
          ),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              SignDialogs.showLoadingDialog(
                  context, "Signing in", _keyLoader); //invoking screens.login
              /*cancelableOperation?.cancel();
              cancelableOperation = CancelableOperation.fromFuture(
                  Future.delayed(Duration(seconds: 1), () {
                setState(() {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                      .pop();
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => MyDashboard()));
                });
              }));*/
              DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
              await FirebaseMessaging.instance.getToken().then((value) {
                deviceToken = value;
              });
              print(deviceToken);
              if (Platform.isIOS) {
                typeOfOS = typeIOS;
                IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
                print('Running on ${iosInfo.utsname.machine}');
                deviceName = iosInfo.utsname.machine;
              } else if (Platform.isAndroid) {
                typeOfOS = typeAndroid;
                AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
                print('Running on ${androidInfo.manufacturer}');
                deviceName = "${androidInfo.manufacturer} ${androidInfo.model}";
              }
              await loginViewModel
                  .userLoginApi(
                      _signin_username.text.trim(),
                      _signin_password.text.trim(),
                      deviceName,
                      typeOfOS,
                      deviceToken)
                  .then((value) {
                LoginModel _loginResponse;
                if (loginViewModel.loginResponse != null) {
                  setState(() {
                    _loginResponse = loginViewModel.loginResponse;
                  });
                }
                if (value) {
                  if (_loginResponse.responseStatus == 1) {
                    SessionManager().login(true);
                    SessionManager().saveUserDetails(
                        jsonEncode(loginViewModel.loginResponse));
                    FocusScope.of(context).requestFocus(new FocusNode());
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                        .pop();
                    Fluttertoast.showToast(
                        msg: loginViewModel.loginResponse.result);
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => MyDashboard()));
                  } else {
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                        .pop();
                    Fluttertoast.showToast(
                        msg: loginViewModel.loginResponse.result);
                  }
                } else {
                  Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                      .pop();
                  Fluttertoast.showToast(msg: tryAgain);
                }
              });
            }
          },
          child: Text(
            titleLogin,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ));
  }

  Widget _buildRegisterText() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: 'Don`t have an account?',
        style: TextStyle(fontSize: 14, color: bottom_nv_bg),
        children: <TextSpan>[
          TextSpan(
            text: ' Register',
            style: TextStyle(
                fontSize: 14, color: bottom_nv_bg, fontWeight: FontWeight.bold),
            recognizer: new TapGestureRecognizer()
              ..onTap = () => /*Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyDashboard()))*/{},
          ),
        ],
      ),
    );
  }
}

class SignDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, String value, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          value + "....",
                          style: TextStyle(color: Colors.white),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
