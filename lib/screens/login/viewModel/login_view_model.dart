import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zingdriver/screens/login/models/login_model.dart';
import 'package:zingdriver/services/repositories.dart';

class LoginViewModel with ChangeNotifier {
  LoginViewModel();

  bool _isHavingData = false;
  LoginModel _loginResponse;

  bool get isHavingData => _isHavingData;

  LoginModel get loginResponse {
    return _loginResponse;
  }

  Future<bool> userLoginApi(String email, String password, String deviceName, String deviceType, String deviceToken) async {
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository().login(email, password, deviceName, deviceToken, deviceType);
      if (response != null) {
        _loginResponse = LoginModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    notifyListeners();
    return _isHavingData;
  }
}
