class OrderAcceptanceModel {
  String deliveryRecordId;
  String employeeId;
  String orderId;
  int responseStatus;
  String result;

  OrderAcceptanceModel(
      {this.deliveryRecordId,
        this.employeeId,
        this.orderId,
        this.responseStatus,
        this.result});

  OrderAcceptanceModel.fromJson(Map<String, dynamic> json) {
    deliveryRecordId = json['deliveryRecordId'];
    employeeId = json['employeeId'];
    orderId = json['orderId'];
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deliveryRecordId'] = this.deliveryRecordId;
    data['employeeId'] = this.employeeId;
    data['orderId'] = this.orderId;
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
