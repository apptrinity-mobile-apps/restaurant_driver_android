class OrderDetailsModel {
  DeliveryOrderDetails deliveryOrderDetails;
  int responseStatus;
  String result;

  OrderDetailsModel(
      {this.deliveryOrderDetails, this.responseStatus, this.result});

  OrderDetailsModel.fromJson(Map<String, dynamic> json) {
    deliveryOrderDetails = json['delivery_order_details'] != null
        ? new DeliveryOrderDetails.fromJson(json['delivery_order_details'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.deliveryOrderDetails != null) {
      data['delivery_order_details'] = this.deliveryOrderDetails.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DeliveryOrderDetails {
  String deliveryInstructions;
  DeliveryLocation deliveryLocation;
  String deliveryLocationAddress;
  String deliveryRecordId;
  double dueAmount;
  String orderId;
  String orderUniqueId;
  DeliveryLocation pickupLocation;
  String pickupLocationAddress;
  String restaurantName;
  double totalAmount;

  DeliveryOrderDetails(
      {this.deliveryInstructions,
        this.deliveryLocation,
        this.deliveryLocationAddress,
        this.deliveryRecordId,
        this.dueAmount,
        this.orderId,
        this.orderUniqueId,
        this.pickupLocation,
        this.pickupLocationAddress,
        this.restaurantName,
        this.totalAmount});

  DeliveryOrderDetails.fromJson(Map<String, dynamic> json) {
    deliveryInstructions = json['deliveryInstructions'];
    deliveryLocation = json['deliveryLocation'] != null
        ? new DeliveryLocation.fromJson(json['deliveryLocation'])
        : null;
    deliveryLocationAddress = json['deliveryLocationAddress'];
    deliveryRecordId = json['deliveryRecordId'];
    dueAmount = json['dueAmount'];
    orderId = json['orderId'];
    orderUniqueId = json['orderUniqueId'];
    pickupLocation = json['pickupLocation'] != null
        ? new DeliveryLocation.fromJson(json['pickupLocation'])
        : null;
    pickupLocationAddress = json['pickupLocationAddress'];
    restaurantName = json['restaurantName'];
    totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deliveryInstructions'] = this.deliveryInstructions;
    if (this.deliveryLocation != null) {
      data['deliveryLocation'] = this.deliveryLocation.toJson();
    }
    data['deliveryLocationAddress'] = this.deliveryLocationAddress;
    data['deliveryRecordId'] = this.deliveryRecordId;
    data['dueAmount'] = this.dueAmount;
    data['orderId'] = this.orderId;
    data['orderUniqueId'] = this.orderUniqueId;
    if (this.pickupLocation != null) {
      data['pickupLocation'] = this.pickupLocation.toJson();
    }
    data['pickupLocationAddress'] = this.pickupLocationAddress;
    data['restaurantName'] = this.restaurantName;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

class DeliveryLocation {
  List<double> coordinates;
  String type;

  DeliveryLocation({this.coordinates, this.type});

  DeliveryLocation.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'].cast<double>();
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['coordinates'] = this.coordinates;
    data['type'] = this.type;
    return data;
  }
}
