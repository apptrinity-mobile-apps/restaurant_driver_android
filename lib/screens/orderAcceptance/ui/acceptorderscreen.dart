import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/screens/orderAcceptance/viewModel/accept_order_view_model.dart';
import 'package:zingdriver/screens/tracking/ui/pickuporderscreen.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/shared_preferences.dart';
import 'package:zingdriver/utils/sizeconfig.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:zingdriver/screens/orderAcceptance/models/order_details_model.dart';

class AcceptOrderScreen extends StatefulWidget {
  final String orderId;
  final String deliveryRecordId;

  const AcceptOrderScreen({this.orderId, this.deliveryRecordId, Key key})
      : super(key: key);

  @override
  _AcceptOrderScreenState createState() => _AcceptOrderScreenState();
}

class _AcceptOrderScreenState extends State<AcceptOrderScreen> {
  double width = 0;
  bool isOrderAccepted = false, isDataLoaded = false;
  String mOrderId = "",
      mDeliveryRecordId = "",
      mEmployeeId = "",
      mPickUpAddress = "",
      mDeliveryAddress = "",
      mPickUpRestaurant = "",
      mTotalDue = "";
  AcceptOrderViewModel viewModel;
  CountDownController mTimerController;
  OrderDetailsModel orderDetailsModel;

  @override
  void initState() {
    viewModel = Provider.of<AcceptOrderViewModel>(context, listen: false);
    mTimerController = CountDownController();
    setState(() {
      mOrderId = widget.orderId;
      mDeliveryRecordId = widget.deliveryRecordId;
    });
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mEmployeeId = value.employeeDetails.id;
        viewModel.getOrderDetailsApi(mEmployeeId, mOrderId, mDeliveryRecordId);
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<AcceptOrderViewModel>(context);
    SizeConfig().init(context);
    width = SizeConfig.screenWidth;
    return WillPopScope(
        child: SafeArea(
          bottom: false,
          top: false,
          child: Scaffold(
              backgroundColor: dashboard_bg,
              appBar: AppBar(
                backgroundColor: bottom_nv_bg,
                title: Text(
                  "Accept Order",
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w600,
                      color: CupertinoColors.white),
                ),
                centerTitle: true,
              ),
              body: orderDetailsView(viewModel)),
        ),
        onWillPop: backHandler);
  }

  Future<bool> backHandler() async {
    return !isOrderAccepted;
  }

  Widget orderDetailsView(AcceptOrderViewModel viewModel) {
    print("widget called");
    bool hasData = viewModel.isHavingData;
    if (hasData) {
      if (viewModel.orderDetailsModel != null) {
        setState(() {
          orderDetailsModel = viewModel.orderDetailsModel;
        });
      }
      if (orderDetailsModel.responseStatus == 1) {
        if (!isDataLoaded) {
          //mTimerController.start();
        }
        setState(() {
          mPickUpAddress = viewModel
              .orderDetailsModel.deliveryOrderDetails.pickupLocationAddress;
          mDeliveryAddress = viewModel
              .orderDetailsModel.deliveryOrderDetails.deliveryLocationAddress;
          mPickUpRestaurant =
              viewModel.orderDetailsModel.deliveryOrderDetails.restaurantName;
          mTotalDue = viewModel.orderDetailsModel.deliveryOrderDetails.dueAmount
              .toString();
          isDataLoaded = true;
        });
        mTimerController.start();
      } else {
        mPickUpAddress = "";
        mDeliveryAddress = "";
        mPickUpRestaurant = "";
        mTotalDue = "0.00";
      }
    } else {
      mPickUpAddress = "";
      mDeliveryAddress = "";
      mPickUpRestaurant = "";
      mTotalDue = "0.00";
    }

    return Container(
      margin: const EdgeInsets.only(top: 30, left: 15, right: 15),
      child: Column(
        children: [
          new Container(
              margin: const EdgeInsets.only(top: 10, left: 0, right: 0),
              child: Card(
                elevation: 5,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                    margin: const EdgeInsets.only(
                                        top: 10, left: 15, right: 0),
                                    child: Text(
                                      pickUp,
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: CupertinoColors.black),
                                    )),
                                new Container(
                                    width: width - 150,
                                    margin: const EdgeInsets.only(
                                        top: 3, left: 15, right: 0),
                                    child: Text(
                                      mPickUpAddress,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                          color: CupertinoColors.black),
                                    )),
                              ]),
                          Column(children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(
                                  top: 10, left: 10, right: 10),
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(80),
                                ),
                              ),
                              child: Center(
                                child: CircularCountDownTimer(
                                  duration: 31,
                                  initialDuration: 0,
                                  controller: mTimerController,
                                  width: 45,
                                  height: 45,
                                  ringColor: Colors.white,
                                  ringGradient: null,
                                  fillColor: Colors.black38,
                                  fillGradient: null,
                                  backgroundColor: Colors.black12,
                                  backgroundGradient: null,
                                  strokeWidth: 3.0,
                                  strokeCap: StrokeCap.round,
                                  textStyle: TextStyle(
                                      fontSize: 15.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  textFormat: CountdownTextFormat.S,
                                  isReverse: true,
                                  isReverseAnimation: false,
                                  isTimerTextShown: true,
                                  autoStart: false,
                                  onStart: () {
                                    print('Countdown Started');
                                  },
                                  onComplete: () {
                                    print('Countdown Ended');
                                    Fluttertoast.showToast(msg: "Time up");
                                    // Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ),
                          ])
                        ],
                      ),
                      Container(
                        width: width - 150,
                        margin: EdgeInsets.only(left: 25, top: 10),
                        child: MySeparator(color: dotted_line),
                      ),
                      Container(
                          margin: const EdgeInsets.only(
                              top: 10, left: 25, right: 0),
                          child: Text(
                            dropOff,
                            style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                color: CupertinoColors.black),
                          )),
                      Container(
                          width: width - 150,
                          margin:
                              const EdgeInsets.only(top: 3, left: 25, right: 0),
                          child: Text(
                            mDeliveryAddress,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          )),
                      Container(
                        width: width - 150,
                        margin: EdgeInsets.only(left: 25, top: 10),
                        child: MySeparator(color: dotted_line),
                      ),
                      Container(
                          margin: const EdgeInsets.only(
                              top: 10, left: 25, right: 0),
                          child: Text(
                            deliveryBy,
                            style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          )),
                      Container(
                          margin:
                              const EdgeInsets.only(top: 3, left: 25, right: 0),
                          child: Text(
                            mPickUpRestaurant,
                            style: TextStyle(
                                fontSize: 17,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w700,
                                color: CupertinoColors.black),
                          )),
                      new Container(
                          alignment: Alignment.centerLeft,
                          color: kToolbarTitleColor,
                          height: 56,
                          width: double.infinity,
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.only(left: 25),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                              children: <TextSpan>[
                                TextSpan(text: '\u0024$mTotalDue'),
                                TextSpan(
                                  text: '( Total Bill Amount )',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          )),
                    ]),
              )),
          Container(
            margin: EdgeInsets.only(top: 25, left: 5, right: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    flex: 5,
                    child: Container(
                        height: 56,
                        child:
                            isOrderAccepted ? pickUpButton() : acceptButton())),
                Expanded(flex: 1, child: SizedBox(height: 20)),
                Expanded(
                    flex: 5,
                    child: Container(
                        height: 56,
                        child:
                            isOrderAccepted ? declineButton() : cancelButton()))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget acceptButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: kToolbarTitleColor, // background
        onPrimary: kToolbarTitleColor, // foreground
      ),
      child: Text(
        acceptOrder,
        style: TextStyle(
            fontSize: 16,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w400,
            color: CupertinoColors.white),
      ),
      onPressed: () {
        viewModel
            .orderAcceptanceApi(
                mEmployeeId, mOrderId, mDeliveryRecordId, "accept")
            .then((value) {
          if (value) {
            if (viewModel.orderAcceptanceModel.responseStatus == 1) {
              Fluttertoast.showToast(msg: viewModel.result);
              setState(() {
                isOrderAccepted = true;
              });
            } else {
              Fluttertoast.showToast(msg: tryAgain);
            }
          } else {
            Fluttertoast.showToast(msg: tryAgain);
          }
        });
      },
    );
  }

  Widget pickUpButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: kToolbarTitleColor, // background
        onPrimary: kToolbarTitleColor, // foreground
      ),
      child: Text(
        pickUpOrder,
        style: TextStyle(
            fontSize: 16,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w400,
            color: CupertinoColors.white),
      ),
      onPressed: () {
        viewModel
            .orderAcceptanceApi(
                mEmployeeId, mOrderId, mDeliveryRecordId, "pickup")
            .then((value) {
          if (value) {
            if (viewModel.orderAcceptanceModel.responseStatus == 1) {
              Fluttertoast.showToast(msg: viewModel.result);
              // clearing all pages and navigating to dashboard
              print("saving ${orderDetailsModel.deliveryOrderDetails}");
              SessionManager().isDelivering(true);
              SessionManager().saveOrderDetails(jsonEncode(orderDetailsModel));
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => PickupOrderScreen()),
                  (Route<dynamic> route) => false);
            } else {
              Fluttertoast.showToast(msg: tryAgain);
            }
          } else {
            Fluttertoast.showToast(msg: tryAgain);
          }
        });
      },
    );
  }

  Widget cancelButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white, // background
        onPrimary: Colors.white, // foreground
      ),
      child: Text(
        cancelOrder,
        style: TextStyle(
            fontSize: 16,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w400,
            color: kToolbarTitleColor),
      ),
      onPressed: () {
        viewModel
            .orderAcceptanceApi(
                mEmployeeId, mOrderId, mDeliveryRecordId, "cancel")
            .then((value) {
          if (value) {
            if (viewModel.orderAcceptanceModel.responseStatus == 1) {
              Fluttertoast.showToast(msg: viewModel.result);
              Navigator.of(context).pop();
            } else {
              Fluttertoast.showToast(msg: tryAgain);
            }
          } else {
            Fluttertoast.showToast(msg: tryAgain);
          }
        });
      },
    );
  }

  Widget declineButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white, // background
        onPrimary: Colors.white, // foreground
      ),
      child: Text(
        declineOrder,
        style: TextStyle(
            fontSize: 16,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w400,
            color: kToolbarTitleColor),
      ),
      onPressed: () {
        viewModel
            .orderAcceptanceApi(
                mEmployeeId, mOrderId, mDeliveryRecordId, "decline")
            .then((value) {
          if (value) {
            if (viewModel.orderAcceptanceModel.responseStatus == 1) {
              Fluttertoast.showToast(msg: viewModel.result);
              Navigator.of(context).pop();
            } else {
              Fluttertoast.showToast(msg: tryAgain);
            }
          } else {
            Fluttertoast.showToast(msg: tryAgain);
          }
        });
      },
    );
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 2.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
