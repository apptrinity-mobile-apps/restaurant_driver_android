import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:zingdriver/screens/orderAcceptance/models/order_acceptance_model.dart';
import 'package:zingdriver/screens/orderAcceptance/models/order_details_model.dart';
import 'package:zingdriver/services/repositories.dart';

class AcceptOrderViewModel with ChangeNotifier {
  AcceptOrderViewModel();

  bool _isHavingData = false;
  OrderDetailsModel _orderDetailsModel;
  OrderAcceptanceModel _orderAcceptanceModel;
  String _result = "";

  bool get isHavingData => _isHavingData;
  String get result => _result;

  OrderDetailsModel get orderDetailsModel {
    return _orderDetailsModel;
  }

  OrderAcceptanceModel get orderAcceptanceModel {
    return _orderAcceptanceModel;
  }

  Future<bool> getOrderDetailsApi(
      String employeeId, String orderId, String deliveryRecordId) async {
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .orderDetails(employeeId, orderId, deliveryRecordId);
      if (response != null) {
        _orderDetailsModel = OrderDetailsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      print(e);
      _isHavingData = false;
    }
    notifyListeners();
    return _isHavingData;
  }

  Future<bool> orderAcceptanceApi(String employeeId, String orderId,
      String deliveryRecordId, String decision) async {
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .orderAcceptance(employeeId, orderId, deliveryRecordId, decision);
      if (response != null) {
        _orderAcceptanceModel = OrderAcceptanceModel.fromJson(response);
        _result = _orderAcceptanceModel.result;
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      print(e);
      _isHavingData = false;
    }
    notifyListeners();
    return _isHavingData;
  }
}
