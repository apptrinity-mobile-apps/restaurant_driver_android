import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:maps_curved_line/maps_curved_line.dart';
import 'package:zingdriver/screens/ordersHistory/models/pending_orders_model.dart';
import 'package:zingdriver/utils/all_constans.dart';

class OrderDetailedView extends StatefulWidget {
  final OrdersList orderItem;

  const OrderDetailedView(this.orderItem, {Key key}) : super(key: key);

  @override
  _OrderDetailedViewState createState() => _OrderDetailedViewState();
}

class _OrderDetailedViewState extends State<OrderDetailedView> {
  OrdersList _item;
  Location location = Location();
  Completer<GoogleMapController> _controllerNew = Completer();
  Set<Marker> _markers = {};
  Set<Polyline> _polylines = {};
  LatLng DELIVERY_LOCATION, PICKUP_LOCATION;
  BitmapDescriptor pinDeliveryLocationIcon, pinPickupLocationIcon;
  LatLng _initialcameraposition = LatLng(0.0, 0.0);
  String mapStyle = "";

  @override
  void initState() {
    setCustomMapPin();
    _item = widget.orderItem;
    rootBundle.loadString('map_style/map_style_silver.txt').then((string) {
      mapStyle = string;
    });
    setState(() {
      if (_item.pickupLocation != null) {
        PICKUP_LOCATION = LatLng(_item.pickupLocation.coordinates[0],
            _item.pickupLocation.coordinates[1]
          /*17.442787, 78.368987*/);
      } else {
        PICKUP_LOCATION = LatLng(0.00, 0.00);
      }
      if (_item.deliveryLocation != null) {
        DELIVERY_LOCATION = LatLng(_item.deliveryLocation.coordinates[0],
            _item.deliveryLocation.coordinates[1]
          /*17.4503611, 78.3807887*/);
      } else {
        DELIVERY_LOCATION = LatLng(0.00, 0.00);
      }
      updateMapLocation();
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60,
        elevation: 1,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kToolbarTitleColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: new Padding(
            padding: const EdgeInsets.only(left: 0),
            child: Text(
              deliveryDetails,
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                  color: kToolbarTitleColor),
            )),
        centerTitle: true,
      ),
      body: FutureBuilder(builder: (context, AsyncSnapshot<dynamic> snapshot) {
        return Column(
          children: [
            Expanded(
                child: Container(
                  child: GoogleMap(
                    initialCameraPosition: CameraPosition(
                        target: _initialcameraposition, zoom: trackingMapZoom),
                    mapType: MapType.normal,
                    onMapCreated: (GoogleMapController controller) {
                      controller.setMapStyle(mapStyle);
                      _controllerNew.complete(controller);
                    },
                    myLocationEnabled: false,
                    myLocationButtonEnabled: false,
                    zoomControlsEnabled: false,
                    tiltGesturesEnabled: false,
                    scrollGesturesEnabled: true,
                    compassEnabled: false,
                    mapToolbarEnabled: false,
                    zoomGesturesEnabled: false,
                    rotateGesturesEnabled: false,
                    liteModeEnabled: false,
                    trafficEnabled: false,
                    markers: _markers,
                    polylines: _polylines,
                  ),
                )),
            Expanded(
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Card(
                          elevation: 3,
                          color: kToolbarTitleColor,
                          child: Container(
                              alignment: Alignment.centerLeft,
                              width: double.infinity,
                              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                              child: Text(
                                '$labelOrderId ${_item.orderUniqueId}',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white),
                              )),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        child: Card(
                          elevation: 3,
                          color: kToolbarTitleColor,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                            margin:
                                            EdgeInsets.fromLTRB(10, 10, 10, 0),
                                            padding:
                                            EdgeInsets.fromLTRB(10, 10, 10, 0),
                                            child: Row(
                                              children: [
                                                Image.asset(
                                                  'images/pickup_pin.png',
                                                  height: 20,
                                                ),
                                                Text(
                                                  pickUp,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight
                                                          .w600,
                                                      color: CupertinoColors
                                                          .white),
                                                )
                                              ],
                                            )),
                                      ],
                                    ),
                                    Container(
                                        margin: EdgeInsets.fromLTRB(
                                            10, 5, 10, 5),
                                        padding: EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: Text(
                                          _item.pickupLocationAddress,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: CupertinoColors.white),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          margin:
                                          EdgeInsets.fromLTRB(10, 10, 10, 0),
                                          padding:
                                          EdgeInsets.fromLTRB(10, 0, 10, 0),
                                          child: Row(children: [
                                            Image.asset(
                                              'images/drop_pin.png',
                                              height: 20,
                                            ),
                                            Text(
                                              dropOff,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                  color: CupertinoColors.white),
                                            )
                                          ]),
                                        )
                                      ],
                                    ),
                                    Container(
                                        margin: EdgeInsets.fromLTRB(
                                            10, 5, 10, 5),
                                        padding: EdgeInsets.fromLTRB(
                                            10, 0, 10, 0),
                                        child: Text(
                                          _item.deliveryLocationAddress,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: CupertinoColors.white),
                                        )),
                                  ],
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                                  child: RichText(
                                    textAlign: TextAlign.start,
                                    text: TextSpan(
                                      text: deliveredOrderFrom,
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: CupertinoColors.white),
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: _item.restaurantName,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                              color: CupertinoColors.white),
                                        ),
                                      ],
                                    ),
                                  )),
                              Container(
                                  color: Colors.white,
                                  alignment: Alignment.centerLeft,
                                  width: double.infinity,
                                  padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                  child: RichText(
                                    text: TextSpan(
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w700,
                                          color: kToolbarTitleColor),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: '\u0024 ${_item
                                                .totalAmount} '),
                                        TextSpan(
                                          text: '($totalBillAmount)',
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                              color: kToolbarTitleColor),
                                        ),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        );
      }),
    );
  }

  Future<void> updateMapLocation() async {
    setMapPins();
    final GoogleMapController controller = await _controllerNew.future;
    LatLngBounds bound =
    LatLngBounds(southwest: PICKUP_LOCATION, northeast: DELIVERY_LOCATION);
    controller.animateCamera(CameraUpdate.newLatLngBounds(bound, 50));
  }

  void setMapPins() async {
    setState(() {
      // source pin
      _markers.add(Marker(
          markerId: MarkerId('sourcePin'),
          position: PICKUP_LOCATION,
          icon: pinPickupLocationIcon,
          onTap: () {}));
      // destination pin
      _markers.add(Marker(
          markerId: MarkerId('destPin'),
          position: DELIVERY_LOCATION,
          icon: pinDeliveryLocationIcon,
          onTap: () {}));
      // path
      _polylines.add(Polyline(
        polylineId: PolylineId('deliveryPath'),
        visible: true,
        width: 2,
        patterns: [PatternItem.dash(30), PatternItem.gap(10)],
        points: MapsCurvedLines.getPointsOnCurve(
            PICKUP_LOCATION, DELIVERY_LOCATION),
        color: Colors.black45,
      ));
    });
  }

  void setCustomMapPin() async {
    pinPickupLocationIcon = BitmapDescriptor.defaultMarker;
    // setting custom marker pin
    /*await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
            // size: Size(32, 32),
        ), 'images/pickup_pin.png');*/
    pinDeliveryLocationIcon = BitmapDescriptor.defaultMarkerWithHue(30);
    // setting custom marker pin
    /*await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          // devicePixelRatio: 2.5,
            size: Size(32, 32),
        ), 'assets/images/drop_pin.png');*/
  }
}
