import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:zingdriver/screens/orderAcceptance/models/order_details_model.dart';
import 'package:zingdriver/services/repositories.dart';

class OrderDetailedViewModel with ChangeNotifier {
  OrderDetailedViewModel();

  OrderDetailsModel _orderDetailsModel;

  OrderDetailsModel get orderDetailsModel {
    return _orderDetailsModel;
  }

  Future<OrderDetailsModel> getOrderDetailsApi(
      String employeeId, String orderId, String deliveryRecordId) async {
    _orderDetailsModel = OrderDetailsModel();
    notifyListeners();
    try {
      dynamic response = await Repository()
          .orderDetails(employeeId, orderId, deliveryRecordId);
      if (response != null) {
        _orderDetailsModel = OrderDetailsModel.fromJson(response);
      } else {
        _orderDetailsModel = OrderDetailsModel();
      }
    } catch (e) {
      print(e);
      _orderDetailsModel = OrderDetailsModel();
    }
    notifyListeners();
    return _orderDetailsModel;
  }
}
