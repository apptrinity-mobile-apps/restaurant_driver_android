class PendingOrdersModel {
  List<OrdersList> ordersList;
  int responseStatus;
  String result;

  PendingOrdersModel({this.ordersList, this.responseStatus, this.result});

  PendingOrdersModel.fromJson(Map<String, dynamic> json) {
    if (json['orders_list'] != null) {
      ordersList = <OrdersList>[];
      json['orders_list'].forEach((v) {
        ordersList.add(new OrdersList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ordersList != null) {
      data['orders_list'] = this.ordersList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class OrdersList {
  String deliveryInstructions;
  DeliveryLocation deliveryLocation;
  String deliveryLocationAddress;
  String deliveryRecordId;
  double dueAmount;
  String orderId;
  String orderUniqueId;
  DeliveryLocation pickupLocation;
  String pickupLocationAddress;
  String restaurantName;
  double totalAmount;

  OrdersList(
      {this.deliveryInstructions,
        this.deliveryLocation,
        this.deliveryLocationAddress,
        this.deliveryRecordId,
        this.dueAmount,
        this.orderId,
        this.orderUniqueId,
        this.pickupLocation,
        this.pickupLocationAddress,
        this.restaurantName,
        this.totalAmount});

  OrdersList.fromJson(Map<String, dynamic> json) {
    deliveryInstructions = json['deliveryInstructions'];
    deliveryLocation = json['deliveryLocation'] != null
        ? new DeliveryLocation.fromJson(json['deliveryLocation'])
        : null;
    deliveryLocationAddress = json['deliveryLocationAddress'];
    deliveryRecordId = json['deliveryRecordId'];
    dueAmount = json['dueAmount'];
    orderId = json['orderId'];
    orderUniqueId = json['orderUniqueId'];
    pickupLocation = json['pickupLocation'] != null
        ? new DeliveryLocation.fromJson(json['pickupLocation'])
        : null;
    pickupLocationAddress = json['pickupLocationAddress'];
    restaurantName = json['restaurantName'];
    totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deliveryInstructions'] = this.deliveryInstructions;
    if (this.deliveryLocation != null) {
      data['deliveryLocation'] = this.deliveryLocation.toJson();
    }
    data['deliveryLocationAddress'] = this.deliveryLocationAddress;
    data['deliveryRecordId'] = this.deliveryRecordId;
    data['dueAmount'] = this.dueAmount;
    data['orderId'] = this.orderId;
    data['orderUniqueId'] = this.orderUniqueId;
    if (this.pickupLocation != null) {
      data['pickupLocation'] = this.pickupLocation.toJson();
    }
    data['pickupLocationAddress'] = this.pickupLocationAddress;
    data['restaurantName'] = this.restaurantName;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

class DeliveryLocation {
  List<double> coordinates;
  String type;

  DeliveryLocation({this.coordinates, this.type});

  DeliveryLocation.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'].cast<double>();
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['coordinates'] = this.coordinates;
    data['type'] = this.type;
    return data;
  }
}
