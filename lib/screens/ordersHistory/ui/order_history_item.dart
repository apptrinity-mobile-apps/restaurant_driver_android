import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:zingdriver/screens/orderAcceptance/ui/acceptorderscreen.dart';
import 'package:zingdriver/screens/orderDetails/ui/order_detailed_view.dart';
import 'package:zingdriver/screens/ordersHistory/models/pending_orders_model.dart';
import 'package:zingdriver/utils/all_constans.dart';

class OrderHistoryItem extends StatefulWidget {
  final OrdersList ordersModel;
  final String type;

  const OrderHistoryItem(this.type, this.ordersModel, {Key key})
      : super(key: key);

  @override
  _OrderHistoryItemState createState() => _OrderHistoryItemState();
}

class _OrderHistoryItemState extends State<OrderHistoryItem> {
  OrdersList _item;
  String _type;

  @override
  void initState() {
    _item = widget.ordersModel;
    _type = widget.type;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(3),
      child: Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Colors.black,
            width: 0.1,
          ),
        ),
        child: Container(
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                margin:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: Text(
                                  pickUp,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                      color: CupertinoColors.black),
                                )),
                            Container(
                                margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: Text(
                                  _item.pickupLocationAddress,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                      color: CupertinoColors.black),
                                )),
                          ],
                        ),
                      ),
                      flex: 2,
                    ),
                    Expanded(
                      child: Container(
                          margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                // navigate to details screen
                                _type == "pickup"
                                    ? Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AcceptOrderScreen(
                                                  orderId: _item.orderId,
                                                  deliveryRecordId:
                                                      _item.deliveryRecordId,
                                                )))
                                    : Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                OrderDetailedView(
                                                  _item,
                                                )));
                              },
                              child: Text(
                                viewDetails,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    decorationStyle: TextDecorationStyle.dashed,
                                    fontSize: 13,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                    color: CupertinoColors.black),
                              ),
                            ),
                          )),
                      flex: 1,
                    ),
                  ],
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: Text(
                            dropOff,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                color: CupertinoColors.black),
                          )),
                      Container(
                          margin: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                          child: Text(
                            _item.deliveryLocationAddress,
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                                color: CupertinoColors.black),
                          )),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  color: history_bottom,
                  child: Container(
                      margin: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      child: RichText(
                        textAlign: TextAlign.start,
                        text: TextSpan(
                          text: _type == "pickup"
                              ? pickedOrderFrom
                              : deliveredOrderFrom,
                          style: TextStyle(fontSize: 13, color: bottom_nv_bg),
                          children: <TextSpan>[
                            TextSpan(
                              text: _item.restaurantName,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                  color: CupertinoColors.black),
                            ),
                          ],
                        ),
                      )),
                ),
              ],
            )),
      ),
    );
  }
}
