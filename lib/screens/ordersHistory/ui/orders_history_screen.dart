import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/screens/ordersHistory/models/pending_orders_model.dart';
import 'package:zingdriver/screens/ordersHistory/ui/order_history_item.dart';
import 'package:zingdriver/screens/ordersHistory/viewModels/orders_history_view_model.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/shared_preferences.dart';

class OrdersHistoryScreen extends StatefulWidget {
  const OrdersHistoryScreen({Key key}) : super(key: key);

  @override
  _OrdersHistoryScreenState createState() => _OrdersHistoryScreenState();
}

class _OrdersHistoryScreenState extends State<OrdersHistoryScreen>
    with AutomaticKeepAliveClientMixin<OrdersHistoryScreen>,TickerProviderStateMixin {
  String mEmployeeId = "";

  @override
  void initState() {
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mEmployeeId = value.employeeDetails.id;
      });
    });
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    final viewModel =
        Provider.of<OrdersHistoryViewModel>(context, listen: true);
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 60,
          elevation: 1,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: kToolbarTitleColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: new Padding(
              padding: const EdgeInsets.only(left: 0),
              child: Text(
                titleDeliveryBox,
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w700,
                    color: kToolbarTitleColor),
              )),
          centerTitle: true,
        ),
        body: DefaultTabController(
          length: 2,
          initialIndex: 1,
          child: Scaffold(
            primary: false,
            appBar: PreferredSize(
              preferredSize: Size(0, 60),
              child: Container(
                child: TabBar(
                  tabs: [
                    Container(
                        padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                        child: Text(pending,style: TextStyle(fontSize: 14),)),
                    Container(
                        padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                        child: Text(completed,style: TextStyle(fontSize: 14),)),
                  ],
                  indicatorWeight: 0,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(0), // Creates border
                      color: kToolbarTitleColor),
                  labelColor: Colors.white,
                  unselectedLabelColor: kToolbarTitleColor,
                ),
                decoration: BoxDecoration(
                    border: Border.all(color: kToolbarTitleColor)),
              ),
            ),
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                pendingOrdersView(viewModel),
                completedOrdersView(viewModel),
              ],
            ),
          ),
        ));
  }

  Widget pendingOrdersView(OrdersHistoryViewModel viewModel) {
    return FutureBuilder(
        future: viewModel.pendingOrdersApi(mEmployeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print(snapshot.connectionState);
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: Container(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 18),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as PendingOrdersModel;
              if (data.responseStatus == 1) {
                if (data.ordersList.length > 0) {
                  return Container(
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: data.ordersList.length,
                        itemBuilder: (context, position) {
                          return OrderHistoryItem(
                              "pickup", data.ordersList[position]);
                        }),
                  );
                } else {
                  return Center(
                    child: Text(
                      noPendingOrders,
                      style: TextStyle(fontSize: 18),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 18),
                  ),
                );
              }
            } else {
              return Container(
                child: Center(
                  child: Text(
                    tryAgain,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              );
            }
          } else {
            return Center(
              child: Container(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }

  Widget completedOrdersView(OrdersHistoryViewModel viewModel) {
    return FutureBuilder(
        future: viewModel.completedOrdersApi(mEmployeeId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: Container(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 18),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as PendingOrdersModel;
              if (data.responseStatus == 1) {
                if (data.ordersList.length > 0) {
                  return Container(
                    child: ListView.builder(
                        itemCount: data.ordersList.length,
                        itemBuilder: (context, position) {
                          return OrderHistoryItem(
                              "completed", data.ordersList[position]);
                        }),
                  );
                } else {
                  return Center(
                    child: Text(
                      noCompletedOrders,
                      style: TextStyle(fontSize: 18),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 18),
                  ),
                );
              }
            } else {
              return Container(
                child: Center(
                  child: Text(
                    tryAgain,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              );
            }
          } else {
            return Center(
              child: Container(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }
}
