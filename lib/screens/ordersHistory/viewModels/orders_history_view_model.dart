import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zingdriver/screens/ordersHistory/models/pending_orders_model.dart';
import 'package:zingdriver/services/repositories.dart';

class OrdersHistoryViewModel with ChangeNotifier {
  OrdersHistoryViewModel();

  bool _isHavingData = false;
  PendingOrdersModel _pendingOrdersModel;

  bool get isHavingData => _isHavingData;

  PendingOrdersModel get pendingOrdersModel {
    return _pendingOrdersModel;
  }

  Future<PendingOrdersModel> completedOrdersApi(String employeeId) async {
    _isHavingData = false;
    _pendingOrdersModel = PendingOrdersModel();
    try {
      dynamic response = await Repository().completedOrders(employeeId);
      if (response != null) {
        _pendingOrdersModel = PendingOrdersModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    return _pendingOrdersModel;
  }

  Future<PendingOrdersModel> pendingOrdersApi(String employeeId) async {
    _isHavingData = false;
    _pendingOrdersModel = PendingOrdersModel();
    try {
      dynamic response = await Repository().pendingOrders(employeeId);
      if (response != null) {
        _pendingOrdersModel = PendingOrdersModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    return _pendingOrdersModel;
  }

}
