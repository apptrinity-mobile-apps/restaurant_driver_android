import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:zingdriver/screens/dashboard/ui/dashboard_main.dart';
import 'package:zingdriver/screens/login/ui/login.dart';
import 'package:zingdriver/screens/tracking/ui/pickuporderscreen.dart';
import 'package:zingdriver/utils/shared_preferences.dart';
import 'package:zingdriver/utils/sizeconfig.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen() : super();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isUserLoggedIn = false, isUserDelivering = false;

  @override
  void initState() {
    SessionManager().isLoggedIn().then((value) {
      setState(() {
        isUserLoggedIn = value;
        SessionManager().deliveringStatus().then((value) {
          setState(() {
            isUserDelivering = value;
          });
          print(
              "isUserLoggedIn ravi - $isUserLoggedIn====isUserDelivering - $isUserDelivering");
          isUserLoggedIn
              ? isUserDelivering
              ? Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => PickupOrderScreen()),
                  (Route<dynamic> route) => false)
              : Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => MyDashboard()),
                  (Route<dynamic> route) => false)
              : Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
                  (Route<dynamic> route) => false);
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff6f8fb),
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0xffF6F8FB),
            statusBarIconBrightness: Brightness.dark),
        backwardsCompatibility: false,
      ),
      body: SafeArea(
        child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(8),
            color: Colors.white,
            child: FlutterLogo(size: SizeConfig.screenHeight)),
      ),
    );
  }
}
