import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:zingdriver/main.dart';
import 'package:zingdriver/screens/tracking/viewModel/tracking_view_model.dart';
import 'package:zingdriver/utils/all_constans.dart';
import 'package:zingdriver/utils/shared_preferences.dart';
import 'package:zingdriver/utils/sizeconfig.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class PickupOrderScreen extends StatefulWidget {
  @override
  _PickupOrderScreenState createState() => _PickupOrderScreenState();
}

class _PickupOrderScreenState extends State<PickupOrderScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  LocationData _currentPosition;
  Marker marker;
  Location location = Location();
  GoogleMapController _controller;
  LatLng _initialcameraposition = LatLng(17.4433499, 78.3839749);
  LatLng latlong;
  BitmapDescriptor pinDeliveryLocationIcon, pinPickupLocationIcon;
  final now = Duration(seconds: 30);
  Set<Marker> _markers = {};
  bool isSwitched = false;
  final List<LatLng> polyPoints = []; // For holding Co-ordinates as LatLng
  final Set<Polyline> polyLines = {}; //
  int isteps = 0;
  bool startStop = true;
  Stopwatch watch = Stopwatch();
  Timer timer;
  String elapsedTime = '00 : 00 : 00';
  String starttime = "";
  String endtime = "";
  String token = "";
  String username = "";
  int _selectedDestination = 0;
  String themetype = "";
  double height = 0.0, mDueAmount = 0.0;
  String user_available_credits = "";
  String user_achieve_kms = "";
  String user_achieve_steps = "";
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  String onlineStatus = "",
      mEmployeeId = "",
      mOrderId = "",
      orderUniqueId = "",
      mDeliveryRecordId = "";
  int switchedStatus = 0;

  PolylinePoints polylinePoints = PolylinePoints();
  Map<PolylineId, Polyline> polylines = {};
  double _originLatitude = 0.0,
      _originLongitude = 0.0,
      _destLatitude = 17.4435,
      _destLongitude = 78.3772;
  LatLng DEST_LOCATION, SOURCE_LOCATION;

  updateTime(Timer timer) {
    if (watch.isRunning) {
      setState(() {
        print("startstop Inside=$startStop");
        elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getLoc();
    setCustomMapPin();
    SessionManager().getUserDetails().then((value) {
      setState(() {
        mEmployeeId = value.employeeDetails.id;
      });
    });
    SessionManager().getOrderDetails().then((value) {
      setState(() {
        mOrderId = value.deliveryOrderDetails.orderId;
        orderUniqueId = value.deliveryOrderDetails.orderUniqueId;
        mDueAmount = value.deliveryOrderDetails.dueAmount;
        mDeliveryRecordId = value.deliveryOrderDetails.deliveryRecordId;
        if (value.deliveryOrderDetails.pickupLocation != null) {
          SOURCE_LOCATION = LatLng(
              value.deliveryOrderDetails.pickupLocation.coordinates[0],
              value.deliveryOrderDetails.pickupLocation.coordinates[1]
              /*17.442787,
              78.368987*/
              );
        } else {
          SOURCE_LOCATION = LatLng(0.00, 0.00);
        }
        if (value.deliveryOrderDetails.deliveryLocation != null) {
          DEST_LOCATION = LatLng(
              value.deliveryOrderDetails.deliveryLocation.coordinates[0],
              value.deliveryOrderDetails.deliveryLocation.coordinates[1]
              /*17.4503611,
              78.3807887*/
              );
        } else {
          DEST_LOCATION = LatLng(0.00, 0.00);
        }
        _originLatitude = SOURCE_LOCATION.latitude;
        _originLongitude = SOURCE_LOCATION.longitude;
        _destLatitude = DEST_LOCATION.latitude;
        _destLongitude = DEST_LOCATION.longitude;
      });
      print("$SOURCE_LOCATION +++++ $DEST_LOCATION");
    });
    _getPolyline();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void setCustomMapPin() async {
    pinPickupLocationIcon =
        /*await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
            // size: Size(32, 32),
        ), 'images/pickup_pin.png');*/
        BitmapDescriptor.defaultMarker;
    pinDeliveryLocationIcon =
        /*await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 2.5,
            // size: Size(32, 32),
        ), 'images/drop_pin.png');*/
        BitmapDescriptor.defaultMarkerWithHue(90);
  }

  void setMapPins() {
    setState(() {
      // source pin
      _markers.add(Marker(
          markerId: MarkerId('sourcePin'),
          position: SOURCE_LOCATION,
          icon: pinPickupLocationIcon));
      // destination pin
      _markers.add(Marker(
          markerId: MarkerId('destPin'),
          position: DEST_LOCATION,
          icon: pinDeliveryLocationIcon));
    });
    print("sizee" + _markers.length.toString());
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    //_controller.setMapStyle(MapType.normal);
    location.onLocationChanged.listen((l) {
      print("test${l.latitude} : ${l.longitude}");
      _originLatitude = l.latitude;
      _originLongitude = l.longitude;
      latlong = new LatLng(l.latitude, l.longitude);
      if (startStop == false) {
        polyPoints.add(LatLng(l.latitude, l.longitude));
        if (polyPoints.length > 0) {
          if (mounted)
            setState(() {
              setPolyLines();
            });
        }
      }
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
        ),
      );
      setMapPins();
      /*_markers.add(Marker(
        markerId: MarkerId("a"),
        draggable: false,
        position: DEST_LOCATION,
        icon: pinDeliveryLocationIcon,
        // onDragEnd: (_currentlatLng) {
        //     latlong = _currentlatLng;
        //   }
      ));*/
    });
  }

  setPolyLines() {
    Polyline polyline = Polyline(
      polylineId: PolylineId("polyline"),
      color: Colors.deepOrangeAccent,
      points: polyPoints,
      width: 2,
    );
    polyLines.add(polyline);
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr : $minutesStr : $secondsStr";
  }

  void selectDestination(int index) {
    setState(() {
      _selectedDestination = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = Provider.of<TrackingViewModel>(context, listen: true);
    SizeConfig().init(context);
    print(SizeConfig.screenHeight);
    if (SizeConfig.screenHeight <= 640) {
      height = MediaQuery.of(context).size.height;
    } else {
      height = MediaQuery.of(context).size.height;
    }
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
          key: _scaffoldKey,
          body: Container(
            child: Center(
              child: Column(
                children: [
                  Container(
                    height: height,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                            bottomRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                          ),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            widthFactor: 2.5,
                            child: GoogleMap(
                              initialCameraPosition: CameraPosition(
                                  target: _initialcameraposition,
                                  zoom: trackingMapZoom),
                              mapType: MapType.normal,
                              onMapCreated: _onMapCreated,
                              myLocationEnabled: true,
                              myLocationButtonEnabled: false,
                              zoomControlsEnabled: false,
                              markers: _markers,
                              polylines: Set<Polyline>.of(polylines.values),
                            ),
                          ),
                        ),
                        Positioned(
                          //top: 50,
                          bottom: 30,
                          right: 10,
                          left: 10,
                          child: Card(
                            color: kToolbarTitleColor,
                            elevation: 5,
                            shadowColor: kToolbarTitleColor,
                            child: Column(
                              children: [
                                Container(
                                    alignment: Alignment.centerLeft,
                                    width: double.infinity,
                                    margin: EdgeInsets.fromLTRB(5, 10, 5, 0),
                                    padding:
                                        EdgeInsets.fromLTRB(10, 15, 10, 10),
                                    child: Text(
                                      '$labelOrderId $orderUniqueId',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white),
                                    )),
                                mDueAmount != 0.00
                                    ? Container(
                                        alignment: Alignment.centerLeft,
                                        width: double.infinity,
                                        margin:
                                            EdgeInsets.fromLTRB(5, 10, 5, 0),
                                        padding:
                                            EdgeInsets.fromLTRB(10, 10, 10, 15),
                                        child: RichText(
                                          text: TextSpan(
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w700,
                                                color: Colors.white),
                                            children: <TextSpan>[
                                              TextSpan(
                                                text:
                                                    collectAmountBeforeDelivery,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.white),
                                              ),
                                              TextSpan(
                                                  text: ' \u0024 $mDueAmount'),
                                            ],
                                          ),
                                        ))
                                    : SizedBox(),
                                Container(
                                  margin: EdgeInsets.fromLTRB(15, 10, 15, 20),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white, // background
                                      onPrimary:
                                          kToolbarTitleColor, // foreground
                                    ),
                                    onPressed: () {
                                      orderComplete(viewModel);
                                    },
                                    child: Center(
                                      child: Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 10, 0, 10),
                                        child: Text(
                                          deliveredOrder,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  getLoc() async {
    bool _serviceEnabled;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    await Permission.location.request();
    if (await Permission.location.isGranted) {
      _currentPosition = await location.getLocation();
      _initialcameraposition =
          LatLng(_currentPosition.latitude, _currentPosition.longitude);
      location.onLocationChanged.listen((LocationData currentLocation) {
        print("${currentLocation.latitude} : ${currentLocation.longitude}");
        if (mounted) {
          setState(() {
            _currentPosition = currentLocation;
            _initialcameraposition =
                LatLng(_currentPosition.latitude, _currentPosition.longitude);
            _getAddress(_currentPosition.latitude, _currentPosition.longitude)
                .then((value) {
              setState(() {
                String _address = "${value.first.addressLine}";
              });
            });
          });
        }
      });
    } else if (await Permission.location.status.isDenied) {
      await Permission.location.shouldShowRequestRationale;
    } else if (await Permission.location.status.isPermanentlyDenied) {
      await Permission.location.shouldShowRequestRationale;
    }
  }

  Future<List<Address>> _getAddress(double lat, double lang) async {
    final coordinates = new Coordinates(lat, lang);
    List<Address> add =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    return add;
  }

  /*void createMyMarker() {
    markers.clear();
    final String markerIdVal = 'marker_pick_up';
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
    );
    setState(() {
      markers[markerId] = marker;
    });
  }*/

  _addPolyLine(List<LatLng> polylineCoordinates) {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      points: polylineCoordinates,
      width: 5,
      color: Colors.black54,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
    );
    polylines[id] = polyline;
    setState(() {});
  }

  void _getPolyline() async {
    List<LatLng> polylineCoordinates = [];
    /* PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyBLXX3uV56Ak9tYvB3U-Yes58MaUkNmNSg",
      // "AIzaSyDw76Db3gXoi4D1leCc24swL58kWacqAj4",
      PointLatLng(_originLatitude, _originLongitude),
      PointLatLng(_destLatitude, _destLongitude),
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    } else {
      print("error map" + result.errorMessage);
    }*/
    // TODO delete after directions API key is initialized and uncomment above code.
    polylinePoints
        .decodePolyline(
            "wtmiBkzi}MwHtWyMdI{ChK`LvB~HPrFh@lEYlHsDjD{@iAlEsD~EyAzDbCbDbEfFrDbE~C~E{@^c@r@Px@TVNPB^IZSX[b@GZGl@y@lGKtBCPn@HrCp@Jf@FVVVb@Vl@Lb@J|@uB_@Oa@I")
        .forEach((PointLatLng point) {
      polylineCoordinates.add(LatLng(point.latitude, point.longitude));
    });

    _addPolyLine(polylineCoordinates);
  }

  orderComplete(TrackingViewModel viewModel) async {
    await viewModel
        .orderDeliveredApi(mEmployeeId, mOrderId, mDeliveryRecordId)
        .then((value) {
      if (value) {
        if (viewModel.basicResponse.responseStatus == 1) {
          Fluttertoast.showToast(msg: viewModel.basicResponse.result);
          SessionManager().isDelivering(false);
          SessionManager().saveOrderDetails("");
          // for getting base context, redirecting to base app , i.e., MyApp().
          Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => MyApp()),
              (route) => false);
        }
      } else {
        print("pickup called");
        Fluttertoast.showToast(msg: tryAgain);
      }
    });
  }
}
