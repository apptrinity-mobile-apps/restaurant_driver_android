import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zingdriver/services/basic_response.dart';
import 'package:zingdriver/services/repositories.dart';

class TrackingViewModel with ChangeNotifier {
  bool _isHavingData = false;

  bool get isHavingData => _isHavingData;
  BasicResponse _basicResponse;

  BasicResponse get basicResponse {
    return _basicResponse;
  }

  Future<bool> orderDeliveredApi(
      String employeeId, String orderId, String deliveryRecordId) async {
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository()
          .orderDelivered(employeeId, orderId, deliveryRecordId);
      if (response != null) {
        _basicResponse = BasicResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    notifyListeners();
    return _isHavingData;
  }
}
