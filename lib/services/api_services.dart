import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:zingdriver/services/base_service.dart';
import 'app_exceptions.dart';

class ApiService extends BaseService {
  @override
  Future login(String email, String password, String deviceName,
      String deviceType, String deviceToken) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/driver_login");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "email": email,
            "password": password,
            "deviceName": deviceName,
            "deviceType": deviceType,
            "deviceToken": deviceToken
          }));
      print(json.encode({
        "email": email,
        "password": password,
        "deviceName": deviceName,
        "deviceType": deviceType,
        "deviceToken": deviceToken
      }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future logout(String userId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/logout");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"userId": userId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future orderAcceptance(String employeeId, String orderId,
      String deliveryRecordId, String decision) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/driver_order_acceptance");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "orderId": orderId,
            "deliveryRecordId": deliveryRecordId,
            "decision": decision
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future orderDetails(
      String employeeId, String orderId, String deliveryRecordId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/driver_order_details");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "orderId": orderId,
            "deliveryRecordId": deliveryRecordId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future pendingOrders(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/pending_driver_orders_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future completedOrders(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/completed_driver_orders_list");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future dashboardDetails(String employeeId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/driver_dashboard_details");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future driverStatusUpdate(String employeeId, int status) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/driver_availability_change");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"employeeId": employeeId, "status": status}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @override
  Future orderDelivered(
      String employeeId, String orderId, String deliveryRecordId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/deliver_order_to_customer");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({
            "employeeId": employeeId,
            "orderId": orderId,
            "deliveryRecordId": deliveryRecordId
          }));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw InternalServerException(response.body.toString());
      default:
        throw FetchDataException(
            'Error occurred while communication with server with status code : ${response.statusCode}');
    }
  }
}
