abstract class BaseService {
  // final String baseUrl = "http://3.14.187.24/api/driver/";
  final String baseUrl = "http://18.190.55.150/api/driver/";

  Future<dynamic> login(String email, String password, String deviceName,
      String deviceType, String deviceToken);

  Future<dynamic> logout(String userId);

  Future<dynamic> orderAcceptance(String employeeId, String orderId,
      String deliveryRecordId, String decision);

  Future<dynamic> orderDetails(
      String employeeId, String orderId, String deliveryRecordId);

  Future<dynamic> pendingOrders(String employeeId);

  Future<dynamic> completedOrders(String employeeId);

  Future<dynamic> dashboardDetails(String employeeId);

  Future<dynamic> driverStatusUpdate(String employeeId, int status);

  Future<dynamic> orderDelivered(
      String employeeId, String orderId, String deliveryRecordId);
}
