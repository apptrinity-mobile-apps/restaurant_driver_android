class BasicResponse {
  int responseStatus;
  String result;

  BasicResponse({this.responseStatus, this.result});

  BasicResponse.fromJson(Map<String, dynamic> json) {
    responseStatus =
        json['responseStatus'] == null ? 0 : json['responseStatus'];
    result = json['result'] == null ? 0 : json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
