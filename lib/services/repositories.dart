/* local repository */
import 'package:zingdriver/services/api_services.dart';
import 'package:zingdriver/services/base_service.dart';

class Repository {
  BaseService _service = ApiService();

  Future<dynamic> login(String email, String password, String deviceName,
      String deviceType, String deviceToken) async {
    dynamic response = await _service.login(
        email, password, deviceName, deviceToken, deviceType);
    return response;
  }

  Future<dynamic> logout(String userId) async {
    dynamic response = await _service.logout(userId);
    return response;
  }

  Future<dynamic> orderAcceptance(String employeeId, String orderId,
      String deliveryRecordId, String decision) async {
    dynamic response = await _service.orderAcceptance(
        employeeId, orderId, deliveryRecordId, decision);
    return response;
  }

  Future<dynamic> orderDetails(
      String employeeId, String orderId, String deliveryRecordId) async {
    dynamic response =
        await _service.orderDetails(employeeId, orderId, deliveryRecordId);
    return response;
  }

  Future<dynamic> pendingOrders(String employeeId) async {
    dynamic response = await _service.pendingOrders(employeeId);
    return response;
  }

  Future<dynamic> completedOrders(String employeeId) async {
    dynamic response = await _service.completedOrders(employeeId);
    return response;
  }

  Future<dynamic> dashboardDetails(String employeeId) async {
    dynamic response = await _service.dashboardDetails(employeeId);
    return response;
  }

  Future<dynamic> updateOnlineStatus(String employeeId, int status) async {
    dynamic response = await _service.driverStatusUpdate(employeeId, status);
    return response;
  }

  Future<dynamic> orderDelivered(String employeeId, String orderId, String deliveryRecordId) async {
    dynamic response = await _service.orderDelivered(employeeId, orderId, deliveryRecordId);
    return response;
  }
}
