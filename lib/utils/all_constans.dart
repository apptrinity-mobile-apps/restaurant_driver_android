import 'package:flutter/material.dart';

//colors
const kToolbarTitleColor = Color(0xFF6F6F6F);
const imagebg = Color(0xFFF5F5F5);
const dashboard_bg = Color(0xFFF7F7F7);
const bottom_nv_bg = Color(0xFF3D3D3D);
const dotted_line = Color(0xFFDFDFE0);
const history_bottom = Color(0xFFDBDBDB);

const kLoginTextStyle = TextStyle(
    fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.black87);
const kToolbarTextStyle = TextStyle(
    color: kToolbarTitleColor, fontSize: 25.0, fontWeight: FontWeight.bold);
/*const kWalletTextStyle =TextStyle(color: Colors.black87,fontSize: 15.0,fontWeight: FontWeight.normal);
const kWalletTextBoldStyle =TextStyle(color: Colors.black87,fontSize: 15.0,fontWeight: FontWeight.bold);
const kWalletTextActiceStyle =TextStyle(color: kPrimaryColor,fontSize: 15.0,fontWeight: FontWeight.bold);
const kWalletTotalStyle =TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.bold);
const kSettingsTextStyle =TextStyle(color:  Colors.black87,fontSize: 16.0,fontWeight: FontWeight.bold);*/

//StringS
/*const String base_url= 'http://staging.steeloncall.com/calculate/service/';
const String user_base_url= 'http://staging.steeloncall.com/calculate/userservice/';
const String brand_image_url= 'http://staging.steeloncall.com/media/';*/
const String base_url = 'https://steeloncall.com/calculate/service/';
const String user_base_url = 'https://steeloncall.com/calculate/userservice/';
const String brand_image_url = 'https://steeloncall.com/media/';

const double mapZoom = 14.0;
const double trackingMapZoom = 15.0;

const String keyUserid = 'userid';
const String keyFirstName = 'firstname';
const String keyLastName = 'lastname';
const String keyEmail = 'email';
const String keyMobile = 'mobile';
const String keyCity = 'city';
const String keyRegion = 'region';
const String keyPincode = 'pincode';
const String tryAgain = 'Please try again';
const String pleaseWait = 'Please wait';
const String typeAndroid = 'android';
const String typeIOS = 'ios';
const String online = 'Online';
const String offline = 'Offline';
const String cancelled = 'Cancelled';
const String completed = 'Completed';
const String pending = 'Pending';
const String totalOrders = "Total Order's";
const String pickUp = 'Pick Up';
const String dropOff = 'Drop Off';
const String deliveryBy = 'Delivery by';
const String pickUpOrder = 'Pick Up Order';
const String acceptOrder = 'Accept order';
const String cancelOrder = 'Cancel';
const String declineOrder = 'Decline';
const String titleDashboard = 'DashBoard';
const String titleProfile = 'Profile';
const String titleSettings = 'Settings';
const String titleLogin = 'Login';
const String titleLogout = 'Logout';
const String titleDeliveryBox = 'Delivery Box';
const String deliveredOrder = 'Order Delivered';
const String collectAmountBeforeDelivery = 'Amount to be collected before delivery :';
const String labelOrderId = 'Order ID :';
const String noPendingOrders = 'No Pending Orders';
const String noCompletedOrders = 'No Completed Orders';
const String viewDetails = 'View Details';
const String deliveredOrderFrom = 'Delivered order from ';
const String pickedOrderFrom = 'Picked order from ';
const String deliveryDetails = 'Delivery Details';
const String totalBillAmount = 'Total Bill Amount';
