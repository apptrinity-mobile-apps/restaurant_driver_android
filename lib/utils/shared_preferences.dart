import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zingdriver/screens/login/models/login_model.dart';
import 'package:zingdriver/screens/orderAcceptance/models/order_details_model.dart';

class SessionManager {
  static const String userLoggedIn = "isLoggedIn";
  static const String userLoginDetails = "user_login_details";
  static const String userIsOnline = "user_online";
  static const String orderDelivering = "user_deliver_order";
  static const String orderDetails = "order_details";

  void login(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(userLoggedIn, isLogin);
  }

  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(userLoggedIn) == null
        ? false
        : prefs.getBool(userLoggedIn);
  }

  void saveUserDetails(userDetails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(userLoginDetails, userDetails);
  }

  Future<LoginModel> getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(userLoginDetails) ?? "";
    LoginModel _loginResponse = LoginModel.fromJson(jsonDecode(_details));
    return _loginResponse;
  }

  void online(bool isOnline) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(userIsOnline, isOnline);
  }

  Future<bool> isOnline() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(userIsOnline) == null
        ? false
        : prefs.getBool(userIsOnline);
  }

  void isDelivering(bool isOnline) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(orderDelivering, isOnline);
  }

  Future<bool> deliveringStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(orderDelivering) == null
        ? false
        : prefs.getBool(orderDelivering);
  }

  void saveOrderDetails(order) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(orderDetails, order);
  }

  Future<OrderDetailsModel> getOrderDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(orderDetails) ?? "";
    OrderDetailsModel _orderDetailsModel =
        OrderDetailsModel.fromJson(jsonDecode(_details));
    return _orderDetailsModel;
  }
}
